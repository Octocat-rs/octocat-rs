use crate::model::{
    event_types::{macros::repo_origin, RepoEventInfo},
    issues::{comments::IssueComment, Issue},
    prelude::*,
    pull_requests::events::nested::{Change, IssueChanges},
};

/// <https://docs.github.com/en/developers/webhooks-and-events/webhooks/webhook-events-and-payloads#issues>
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct IssueEvent {
    pub action: IssueAction,
    pub issue: Issue,
    pub changes: Option<IssueChanges>,
    #[serde(flatten)]
    pub event_info: RepoEventInfo,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, EnumString, EnumVariantNames)]
#[strum(serialize_all = "snake_case")]
pub enum IssueAction {
    Opened,
    Edited,
    Deleted,
    Pinned,
    Unpinned,
    Closed,
    Reopened,
    Assigned,
    Unassigned,
    Labeled,
    Unlabeled,
    Locked,
    Unlocked,
    Transferred,
    Milestoned,
    Demilestoned,
}

/// <https://docs.github.com/en/developers/webhooks-and-events/webhooks/webhook-events-and-payloads#issue_comment>
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct IssueCommentEvent {
    pub action: IssueAction,
    pub changes: Option<CommentChanges>,
    pub comment: IssueComment,
    #[serde(flatten)]
    pub event_info: RepoEventInfo,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, EnumString, EnumVariantNames)]
#[strum(serialize_all = "snake_case")]
pub enum IssueCommentAction {
    Created,
    Edited,
    Deleted,
}

// TODO: Move this to some type of shared module as `PullRequestReviewEvent`
// uses it
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct CommentChanges {
    pub body: Option<Change>,
}

repo_origin!(IssueEvent);
repo_origin!(IssueCommentEvent);
