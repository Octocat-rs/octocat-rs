# Commit summary

Creates a simple comment with the changes that happend after every commit.

![example](example.png)

## Running

```
TOKEN=****** USERNAME=****** cargo run
```
