<div align="center">

<img alt="Logo" src=".github/assets/octocat.png" width="500">

<a href="https://github.com/octocat-rs/octocat-rs/actions/workflows/build.yml"><img src="https://github.com/octocat-rs/octocat-rs/actions/workflows/build.yml/badge.svg?branch=main" alt="build status"></a>
<a href="https://matrix.to/#/#octocat:matrix.org" rel="noopener" target="_blank"><img src="https://img.shields.io/badge/Octocat-ffffff?style=flat&logo=Matrix&logoColor=black" alt="Chat on Matrix"></a>
<a href="https://discord.gg/Yq7aDSpfRg"> <img src="https://img.shields.io/discord/947629739219238962?label=&labelColor=6A7EC2&logo=discord&logoColor=ffffff&color=7389D8" alt="Discord Server"> </a>
  
# Octocat-rs

</div>

## About

A GitHub API library written in Rust.
